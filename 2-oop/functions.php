<?php 

    // --------------------------------------------------------
    // ფუნქციები ----------------------------------------------

    // შევამოწმოთ არის თუ არა ორი ადამიანი ერთი და იგივე
    // ანუ ემთხვევა თუ არა მათი პირადი ნომრები
    function are_the_same_person($p_1, $p_2) {
        
        // თუ პირადი ნომრები ემთხვევა
        if ($p_1->get_id() === $p_2->get_id()) {
            // დავაბრუნოთ პასუხი "მართალია" ანუ "true" 
            return true;
        }

        // წინააღმდეგ შემთხვევაში, ანუ თუ პირადი ნომრები არ ემთხვევა
        else {
            // დავაბრუნოთ პასუხი "ტყუილია" ანუ "false"
            return false;
        }
    }

    // დავბეჭდოთ არის თუ არა ორი ადამიანი ერთი და იგივე
    function print_the_same_person($p_1, $p_2) {

        // დავბეჭდოთ გამოძახებული ფუნქცია, თავისი პარამეტრებით
        print "print_the_same_person(".$p_1->get_id().", ".$p_2->get_id()."): <br>";
        
        // თუ მითითებული ადამიანები ერთი და იგივეა
        if (are_the_same_person($p_1, $p_2)) {
            print "თქვენ მიუთითეთ ერთი და იგივე ადამიანი!<br>";
        }

        // წინააღმდეგ შემთხვავაში, ანუ თუ მითითებული ადამიანები არ არიან ერთი და იგივე
        else {
            print "თქვენ მიუთითეთ სხვა და სხვა ადამიანი!<br>";
        }
    }

    // შევადაროთ ორი ადამიანი ასაკით და დავბეჭდოთ უფრო დიდი
    function print_older_person($p_1, $p_2) {

        // დავბეჭდოთ გამოძახებული ფუნქცია, თავისი პარამეტრებით
        print "print_older_person(".$p_1->get_id().", ".$p_2->get_id()."): <br>";

        // თუ მითითებული ადამიანები ერთი და იგივეა
        if (are_the_same_person($p_1, $p_2)) {
            print "თქვენ ერთმანეთს ადარებთ ერთი და იგივე ადამიანს!<br>";
        }

        // წინააღმდეგ შემთხვავაში, ანუ თუ მითითებული ადამიანები არ არიან ერთი და იგივე
        else {

            print "ამ ორ ადამიანს შორის: <br>";

            $p_1->print();
            $p_2->print();
            $age_1 = $p_1->get_age();
            $age_2 = $p_2->get_age();
            
            print "უფრო დიდია: ";

            // თუ პირველი ადამიანია უფრო დიდი
            if ($age_1 > $age_2) {
                $p_1->say_your_name();
            }

            // თუ მეორე ადამიანია უფრო დიდი
            else if ($age_1 < $age_2) {
                $p_2->say_your_name();
            }

            // სხვა შემთხვევაში, ანუ თუ ტოლები არიან
            else {
                print "არცერთი, რადგან ისინი ტოლები არიან!";
            }            
        }
    }

    // დააბრუნე მასივში არსებული ყველაზე მაღალი რიცხვი
    function get_highest_number($array) {
        
        $highest = false;

        // თუ გადმოცემულ მასივში არსებული რიცხვების რაოდენობა 0 არაა გააგრძელე მუშაობა
        // წინააღმდეგ შემთხვევაში დააბრუნე false
        if (count($array)) {

            // საწყის უმაღლეს რიცხვად ვიღებთ მასივის პირველ რიცხვს
            $highest = $array[0];

            // შემდეგ ჩამოვუვლით მასივის დანარჩენ რიცხვებს სათითაოდ ნაბიჯ-ნაბიჯ
            $index = 1;
            while (isset($array[$index])) {

                // და თითოეულ ბიჯზე: თუ უმაღლესი რიცხვი პატარა იქნება მიმდინარე მასივის რიცხვზე
                if ($highest < $array[$index]) {
                    // უმაღლეს რიცხვს ვანაცვლებთ მიმდინარე მასივის რიცხვით
                    $highest = $array[$index];
                }

                $index++;
            } 
        }

        return $highest;
    }

    // დააბრუნე მასივში არსებული ყველაზე დაბალი რიცხვი
    function get_lowest_number($array) {
        
        $lowest = false;
        $length_of_array = count($array);

        // თუ გადმოცემულ მასივში არსებული რიცხვების რაოდენობა 0 არაა გააგრძელე მუშაობა
        // წინააღმდეგ შემთხვევაში დააბრუნე false
        if ($length_of_array) {

            // საწყის უმდაბლეს რიცხვად ვიღებთ მასივის პირველ რიცხვს
            $lowest = $array[0];

            // შემდეგ ჩამოვუვლით მასივის დანარჩენ რიცხვებს სათითაოდ ნაბიჯ-ნაბიჯ
            for ($index = 1; $index < $length_of_array; $index++) {
                
                // და თითოეულ ბიჯზე: თუ უმდაბლესი რიცხვი დიდი იქნება მიმდინარე მასივის რიცხვზე
                if ($lowest > $array[$index]) {
                    // უმდაბლეს რიცხვს ვანაცვლებთ მიმდინარე მასივის რიცხვით
                    $lowest = $array[$index];
                }
            }
        }

        return $lowest;
        
    }

    // დააბრუნე მასივში არსებული რიცხვების ჯამი
    function sum_of_number_array($array = []) {

        $sum = 0;

        // foreach ოპერატორი $number ცვლადში სათითაოდ გადმოგვცემს $array მასივში არსებულ რიცხვებს
        foreach($array as $number) {

            // თითოეულ ბიჯზე ჯამი გავზარდოთ გადმოცემული რიცხვით
            $sum += $number;
        }

        return $sum;
    }

    // გაფილტრე ადამიანების ობიექტების მასივი და დააბრუნე ასაკების მასივი
    function get_ages($persones) {
        return array_map(function($person){
                    return $person->get_age();
                }, $persones);
    }
    
    // ფუნქციები დებაგისთვის ------------------------------------
    //
    // ფუნქცია დებაგისთვის: დაბეჭდე ცვლადი გარკვევით
    function prin($var)
    {
        echo "<pre style='background-color: #fff'>Prin: <br>";
        print_r($var);
        echo "</pre>";
    }
    //
    // ამოაგდე ტექსტი ბრაუზერ შეტყობინების სახით
    function alert($text)
    {
        echo '<script> alert("' . $text . '"); </script>';
    }

    // მონაცემთა ბაზების ტესტირება -----------------
    function testMariaDB($maria, $select) {

        $rows = $maria->select($select, false, PDO::FETCH_BOTH);
        prin($rows); echo $rows[0][0] . "<br>"; echo $rows[0]['id'];

        $rows = $maria->select($select, false, PDO::FETCH_NUM);
        prin($rows); echo $rows[0][0];

        $rows = $maria->select($select, false, PDO::FETCH_ASSOC);
        prin($rows); echo $rows[0]['id'];

        $rows = $maria->select($select, false, PDO::FETCH_OBJ);
        prin($rows); echo $rows[0]->id;

        $rows = $maria->select($select, false, PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Person');
        prin($rows); echo $rows[0]->get_id() . "<br>"; echo $rows[0]->print();

        // $rows = $maria->select($select, false, PDO::FETCH_CLASS, 'Person');
        // prin($rows); echo $rows[0]->get_id() . "<br>"; echo $rows[0]->print();
        
        //echo $rows[0]->print();
    }

?>