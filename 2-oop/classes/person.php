<?php

// include MariaDB
include "maria.php";

class Person {

    private $id;
    private $name;
    private $age;

    // constants
    const TABLE = 'person';
    const ER_INVALID_VALUE = 'value is invalid!';

    // MariaDB object (or instance)
    public static $maria = null;

    // default constructor parameters
    public static $defaultConstructParams = ['0', 'name', 0];

    // true if the Persone is loaded from DB or if I saved it to DB
    private $haveItInDB = false;

    // ----------------------------------------------------------------------------------
    // VALIDATIONS
    private static function validate_id($id) {
        if (preg_match('/^[0-9]+$/', $id))
            return true;
        else 
            return self::invalidValueError(__METHOD__);
    }
    private static function validate_name($name) {
        if (preg_match("/^[a-zA-Z]+$/", $name))
            return true;
        else 
            return self::invalidValueError(__METHOD__);
    }
    private static function validate_age($age) {
        if (($age === 0 || filter_var($age, FILTER_VALIDATE_INT)) && $age >= 0)
            return true;
        else 
            return self::invalidValueError(__METHOD__);
    }
    // ----------------------------------------------------------------------------------
    // SELECT FROM DATABASE
    public static function select($id = false, $fetchType = false) {

        self::$maria = maria::instance();
        $query = 'SELECT * FROM ' . self::TABLE;

        if ($id && self::validate_id($id))
            if ($fetchType)
                return self::$maria->select($query . ' P WHERE P.ID = ?', [$id], $fetchType);
            else
                return self::$maria->select($query . ' P WHERE P.ID = ?', [$id], PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Person');

        else
            if ($fetchType)
                return self::$maria->select($query, false, $fetchType);
            else
                return self::$maria->select($query, false, PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Person');
    }
    // INSERT INTO DATABASE
    public static function insert($id, $name, $age) {

        $res = false;

        if( self::validate_id($id) && 
            self::validate_name($name) && 
            self::validate_age($age)) {

            self::$maria = maria::instance();
            $res = self::_insert($id, $name, $age);
        }

        return $res;
    }
    // UPDATE DATABASE
    public static function update($id, $name, $age) {

        $res = false;

        if( self::validate_id($id) && 
            self::validate_name($name) && 
            self::validate_age($age)) {

            self::$maria = maria::instance();
            $res = self::_update($id, $name, $age);
        }

        return $res;
    }
    //
    public static function delete($id) {
        return self::$maria->execute('DELETE FROM '. self::TABLE . ' WHERE ID = ?', [$id]);
    }
    //
    private static function _insert($id, $name, $age) {
        return self::$maria->execute('INSERT INTO ' . self::TABLE . ' VALUES (?,?,?)', [$id, $name, $age]);
    }
    //
    private static function _update($id, $name, $age) {
        return self::$maria->execute('UPDATE '. self::TABLE . ' P SET P.name = ?, P.age = ? WHERE P.ID = ?', [$name, $age, $id]);
    }

    // ----------------------------------------------------------------------------------
    // OBJECT CONSTRUCTOR
    public function __construct($id, $name = null, $age = null) {

        if (self::$maria === null)
            self::$maria = maria::instance();

        $this->set_id($id);

        if (isset($name) && isset($age)){
            $this->set_name($name);
            $this->set_age($age);
            $this->set_haveItInDB();
        }
        else
            $this->load();
    }

    // ----------------------------------------------------------------------------------
    // load Person's data from DATABASE
    private function load() {

        $rows = self::select($this->id, PDO::FETCH_OBJ);
        
        if (isset($rows[0])) {
            $person = $rows[0];
            $this->set_name($person->name);
            $this->set_age($person->age);
            $this->haveItInDB = true;
        }
    }
    // save Person's data to DATABASE
    public function save()
    {
        if ($this->haveItInDB)
            self::_update($this->id, $this->name, $this->age);
        else
            self::_insert($this->id, $this->name, $this->age);

        $this->haveItInDB = true;
    }
    // remove Person's data from DATABASE
    public function remove() {
        self::delete($this->id);
        $this->haveItInDB = false;
    }

    // ----------------------------------------------------------------------------------
    // Setters     
    private function set_id($id) { // property id cannot be modified from outside of the class
        if (self::validate_id($id)) 
            $this->id = $id;
    }
    public function set_name($name) {
        if (self::validate_name($name)) 
            $this->name = $name;
    }
    public function set_age($age) {
        if (self::validate_age($age))
            $this->age = $age;
    }
    private function set_haveItInDB() {
        $rows = self::select($this->id, PDO::FETCH_OBJ);
        if (isset($rows[0])) 
            $this->haveItInDB = true;
    }
    // Getters 
    public function get_id() {
        return $this->id;
    }
    public function get_name() {
        return $this->name;
    }
    public function get_age() {
        return $this->age;
    }
    
    // ----------------------------------------------------------------------------------
    // დავბეჭდოთ ადამიანის მონაცემები (პირადი ნომერი, სახელი, ასაკი)
    public function print() {
        print "პირადი ნომერი:$this->id სახელი:$this->name ასაკი:$this->age <br>";
    }
    // დავბეჭდოთ ადამიანის სახელი
    public function say_your_name() {
        echo $this->name;
    }

    // ----------------------------------------------------------------------------------
    // error handling
    private static function invalidValueError($method) {
        throw new Exception($method . ': ' . self::ER_INVALID_VALUE);
    }

}

?>