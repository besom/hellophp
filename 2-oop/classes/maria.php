<?php

/**
 * Created by PhpStorm.
 * User: b.Mikaberidze
 * Date: 02/02/2021
 * Time: 12:15 PM
 */

include 'mariaConfig.php';

// singleton pattern
final class maria extends PDO
{
    public $pdo;

    // class instance 
    private static $instance = null;

    // database source name (connection string)
    const DSN = 'mysql:host='.MARIA_HOST.';dbname='.MARIA_DB_NAME.';port='.MARIA_PORT.';charset='.MARIA_CHARSET;

    private $default_options = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
   
    //------------------------------------------------------------------------------------------------------------------
    // get static instance
    public static function instance($options = [])
    {
        if (self::$instance == null)
          self::$instance = new self($options);     

        return self::$instance;
    }

    //------------------------------------------------------------------------------------------------------------------
    private function __construct($options)
    {   
        $options = array_replace($this->default_options, $options);
        parent::__construct(self::DSN, MARIA_USER, MARIA_PASS, $options);
    }

    //------------------------------------------------------------------------------------------------------------------
    public function select($query, $params = false, $fetchType = false, $className = false)
    {
        $rows = [];
        $statement = false;
        $fetchType = $fetchType ? $fetchType : PDO::FETCH_ASSOC;

        if (!$params) {
            $statement = $this->query($query);
        }
        else {
            $statement = $this->prepare($query);
            $statement->execute($params);            
        }

        switch ($fetchType) {

            case PDO::FETCH_CLASS:

                $statement->setFetchMode($fetchType, $className);
                $rows = $statement->fetchAll();
                break;

            case PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE:

                $statement->setFetchMode($fetchType, $className, $className::$defaultConstructParams);
                $rows = $statement->fetchAll();
                break;

            default:

                $rows = $statement->fetchAll($fetchType);
                break;
        }

        return $rows;
    }
    public function execute($query, $params = null) {
        
        $statement = false;

        if (!$params) 
            $statement = $this->exec($query);
        
        else {
            $statement = $this->prepare($query);
            $statement->execute($params);            
        }

        return $statement;
    }

}