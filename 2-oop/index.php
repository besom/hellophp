<?php 

    include './functions.php'; // ფუნქციები
    include './classes/person.php'; // ადამიანის კლასი

    // წამოვიღოთ მონაცემთა ბაზების ობიექქტი
    $maria = maria::instance();

    // --------------------------------------------------------
    // აღვწეროთ რამოდენიმე ადამიანი --------------------------

    echo "შევქმნათ ადამიანის ობიექტი ადამიანის კლასის კონსტრუქტორზე მონაცემების გადაცემით <br>";
    $lu = new Person('01024045', 'lu', 26);
    $lu->print();
    
    // დავტესტოთ ვალიდატორები
    // $goga = new Person('01004043a', 'goga', 25);
    // $goga = new Person('01004043', 'goga1', 25);
    // $goga = new Person('01004043', 'goga', '5a');
    // $goga = new Person('01004043', 'goga', -5);

    echo "<br>პირველ ხაზზე ხელით შექმნილი ადამიანი შევინახოთ ბაზაში <br>";
    $lu->save();
    $lu->print();

    echo "<br>წამოვიღოთ ადამიანების მონაცემები ბაზიდან <br>";
    $persons = $maria->select('select * from person');
    prin($persons);

    echo "<br>პირველ ხაზზე ხელით შექმნილი ადამიანი წავშალოთ ბაზიდან <br>";
    $lu->remove();
    $lu->print();

    echo "<br>წამოვიღოთ ადამიანების მონაცემები ბაზიდან (ადამიანის კლასის გამოყენებით)<br>";
    $persons = Person::select();
    prin($persons);

    echo "<br>წამოვიღოთ კონკრეტული ადამიანის მონაცემები ბაზიდან <br>";
    $personID = '01014042';
    $person = $maria->select('select * from ' . Person::TABLE . ' p where id = ?', [$personID], PDO::FETCH_OBJ);
    prin($person[0]);

    echo "<br>წამოვიღოთ კონკრეტული ადამიანის მონაცემები ბაზიდან და ჩავწეროთ ადამიანის კლასის ობიექტში<br>";
    $personID = '01014042';
    $person = $maria->select('select * from ' . Person::TABLE . ' p where id = ?', [$personID], PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Person');
    $elene = $person[0];
    $elene->print();

    echo "<br>წამოვიღოთ კონკრეტული ადამიანის მონაცემები ბაზიდან  (ადამიანის კლასის გამოყენებით)<br>";
    $personID = '01014042';
    $person = Person::select($personID, PDO::FETCH_BOTH);
    prin($person);

    echo "<br>შევქმნათ ადამიანის ობიექტი ადამიანის კლასის კონსტრუქტორზე მხოლოდ ID_ის გადაცემით (დანარჩენ მონაცემებს ობიექტი წამოიღებს ბაზიდან)<br>";
    $personID = '01024040';
    $zuri = new Person('01024040');
    $zuri->print();

    echo "<br>ადამიანის ობიექტში შევცვალოთ ასაკი და შევინახოთ ცვლილება ბაზაში<br>";
    $zuri->set_age($zuri->get_age()+1);
    $zuri->save();
    $zuri->print();

    $ages = get_ages($persons);

    // თუ მოთხოვნა ლუწ წამს მოვიდა დახატე ყვითელი ფონი, თუარადა ცისფერი
    $bg_color_class = intval(date('s')) % 2 ? 'yello' : 'blue';

?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta name="description" content="Webpage description goes here" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Beso Mikaberidze">
        <meta charset="utf-8">

        <title>Hello PHP</title>

        <link rel="stylesheet" href="css/style.css">
        <script src="http://code.jquery.com/jquery-latest.min.js"></script>

    </head>

    <body>
    
        <div class="container <?php echo $bg_color_class; ?>">

            <h1>Hello PHP</h1>

            <?php
                // ---------------------------------------------------------
                // გამოვიძახოთ ფუნქციები ----------------------------------

                // დავბეჭდოთ ადამიანის მონაცემები სათითაოდ
                echo '===============================================================================================<pre>';
                $zuri->print();
                echo '</pre>===============================================================================================<pre>';
                $lu->print();
                echo '</pre>===============================================================================================<pre>';
                $elene->print();
                echo '</pre>===============================================================================================<pre>';

                // შევადაროთ ადამიანები პირადი ნომრებით
                print_the_same_person($zuri, $lu);
                echo '</pre>===============================================================================================<pre>';
                print_the_same_person($elene, $elene);
                echo '</pre>===============================================================================================<pre>';

                // შევადაროთ ადამიანები ასაკით
                print_older_person($zuri, $lu);
                echo '</pre>===============================================================================================<pre>';
                print_older_person($lu, $zuri);
                echo '</pre>===============================================================================================<pre>';
                print_older_person($elene, $lu);
                echo '</pre>===============================================================================================<pre>';
                print_older_person($elene, $elene);
                echo '</pre>===============================================================================================<pre>';

                print "არსებულ ადამიანებში ყველაზე დიდი ასაკია: " . get_highest_number($ages);
                echo '</pre>===============================================================================================<pre>';
                print "არსებულ ადამიანებში ყველაზე პატარა ასაკია: " . get_lowest_number($ages);
                echo '</pre>===============================================================================================<pre>';
                print "არსებული ადამიანების ჯამური ასაკია: " . sum_of_number_array($ages);
                echo '</pre>===============================================================================================<pre>';

                // დავტესტოთ მონაცემთა ბაზებიდან ინფორმაციის სხვადასხვა სახით წამოღება
                testMariaDB($maria, 'SELECT * FROM person');
            ?>       

        </div>

        <script></script>

    </body>
</html>
