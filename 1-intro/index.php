<?php 

    // ---------------------------------------------------------
    // შემოვიტანოთ ფუნქციები ----------------------------------
    include './functions.php';

    // --------------------------------------------------------
    // აღვწეროთ რამოდენიმე ადამიანი --------------------------
    
    // ადამიანის მონაცემები:
    //// id - პირადი ნომერი
    //// name - სახელი 
    //// age - ასაკი 

    $person_id_1 = '01024040';
    $person_name_1 = 'zura';
    $person_age_1 = 24;
    //
    $person_id_2 = '01014042';
    $person_name_2 = 'aka';
    $person_age_2 = 30;
    //
    $person_id_3 = '01004043';
    $person_name_3 = 'nene';
    $person_age_3 = 24;

    // ყველა ადამანის ასაკი მოვათავსოთ ერთ მასივში
    $ages_array = array( 
        $person_age_1, 
        $person_age_2, 
        $person_age_3);

    // თუ მოთხოვნა ლუწ წამს მოვიდა დახატე ყვითელი ფონი, თუარადა ცისფერი
    $bg_color_class = intval(date('s')) % 2 ? 'yello' : 'blue';

?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta name="description" content="Webpage description goes here" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Beso Mikaberidze">
        <meta charset="utf-8">

        <title>Hello PHP</title>

        <link rel="stylesheet" href="css/style.css">
        <script src="http://code.jquery.com/jquery-latest.min.js"></script>

    </head>

    <body>
    
        <div class="container <?php echo $bg_color_class; ?>">

            <h1>Hello PHP</h1>

            <?php
                // ---------------------------------------------------------
                // გამოვიძახოთ ფუნქციები ----------------------------------

                // დავბეჭდოთ ყველა ადამიანის მონაცემები სათითაოდ
                echo '===============================================================================================<pre>';
                print_person($person_id_1, $person_name_1, $person_age_1);
                echo '</pre>===============================================================================================<pre>';
                print_person($person_id_2, $person_name_2, $person_age_2);
                echo '</pre>===============================================================================================<pre>';
                print_person($person_id_3, $person_name_3, $person_age_3);
                echo '</pre>===============================================================================================<pre>';

                // შევადაროთ ადამიანები პირადი ნომრებით
                print_the_same_person($person_id_1, $person_id_1);
                echo '</pre>===============================================================================================<pre>';
                print_the_same_person($person_id_3, $person_id_2);
                echo '</pre>===============================================================================================<pre>';

                // შევადაროთ ადამიანები ასაკით
                print_older_person($person_id_2, $person_name_2, $person_age_2, $person_id_1, $person_name_1, $person_age_1);
                echo '</pre>===============================================================================================<pre>';
                print_older_person($person_id_1, $person_name_1, $person_age_1, $person_id_2, $person_name_2, $person_age_2);
                echo '</pre>===============================================================================================<pre>';
                print_older_person($person_id_3, $person_name_3, $person_age_3, $person_id_1, $person_name_1, $person_age_1);
                echo '</pre>===============================================================================================<pre>';
                print_older_person($person_id_3, $person_name_3, $person_age_3, $person_id_3, $person_name_3, $person_age_3);
                echo '</pre>===============================================================================================<pre>';

                print "არსებულ ადამიანებში ყველაზე დიდი ასაკია: " . get_highest_number($ages_array);
                echo '</pre>===============================================================================================<pre>';
                print "არსებულ ადამიანებში ყველაზე პატარა ასაკია: " . get_lowest_number($ages_array);
                echo '</pre>===============================================================================================<pre>';
                print "არსებული ადამიანების ჯამური ასაკია: " . sum_of_number_array($ages_array);
                echo '</pre>===============================================================================================<pre>';

            ?>       

        </div>

        <script></script>

    </body>
</html>
