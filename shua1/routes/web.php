<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ----------------------------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------------------

// იმისთვის რომ Route კლასზე გადაცემული კონტროლერების სახელებს სწორი namespace_ით მივაკითხოთ
// app/Providers/RouteServiceProvider.php ფაილში განაკომენტარეთ შემდეგი ხაზი: protected $namespace = 'App\\Http\\Controllers';

// ----------------------------------------------------------------------------------------------------------------------------------------------

Route::view('/welcome', 'welcome');     // return view

Route::redirect('/go', '/to');          // redirect route address '/go' to route address '/to'
Route::redirect('/to', '/home');        // redirect route address '/to' to route address '/home'

Route::get('/', function () {           // redirect route address '/' to route named 'home'
    return redirect('home'); 
    // return redirect()->route('home');
});

// ----------------------------------------------------------------------------------------------------------------------------------------------

Route::prefix('posts')->name('posts.')->group( function () {

    Route::get(     "",              'PostController@index'      )->name('index');        // მაჩვენე ყველა პოსტის გვედრი 

    Route::get(     "create",        'PostController@create'     )->name("create");       // მაჩვენე ახალი პოსტის შესაქმნელი გვერდი
    Route::post(    "",              'PostController@store'      )->name("store");        // შექმენი პოსტი

    Route::get(     "{post}",        'PostController@show'       )->name("show");         // მაჩვენე კონკრეტული პოსტის გვერდი

    Route::get(     "{post}/edit",   'PostController@edit'       )->name("edit");         // მაჩვენე კონკრეტული პოსტის შესაცვლელი გვერდი
    Route::put(     "{post}",        'PostController@update'     )->name("update");       // შეცვალე კონკრეტული პოსტი

    Route::delete(  "{post}",        'PostController@destroy'    )->name("destroy");      // წაშალე კონკრეტული პოსტი

    Route::post(    "ajax",          'PostController@indexAjax'  )->name("ajax");

});

// ----------------------------------------------------------------------------------------------------------------------------------------------

Route::middleware('auth')->group( function () {

    Route::get(     '/home',            'HomeController@index'          )->name('home');
    Route::get(     '/notifications',   'NotificationsController@show'  )->name('notifications.show');

});

// ----------------------------------------------------------------------------------------------------------------------------------------------

Route::get(     '/contact',         'ContactController@show'        )->name('contact.show');
Route::post(    '/contact',         'ContactController@store'       )->name('contact.store');

// ----------------------------------------------------------------------------------------------------------------------------------------------

Auth::routes();

// ----------------------------------------------------------------------------------------------------------------------------------------------

Route::fallback(function () { echo "გვერდი ვერ მოიძებნა"; });