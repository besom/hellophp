<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('posts',                 'PostApiController@index');
Route::get('post/{post}',           'PostApiController@show');

Route::middleware('auth.basic')->group( function() {

    Route::get('tags',                  'TagApiController@index');
    Route::get('tag/{tag}/posts',       'TagApiController@posts');

});

Route::middleware('auth:api')->group( function() {

    Route::post('authors',               'UserApiController@index');
    Route::post('author/{author}/posts', 'UserApiController@posts');
    Route::get ('author/{author}/posts', 'UserApiController@posts');

});