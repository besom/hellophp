@extends('layouts.layout')

@section('content')

    <script>

        function getPosts() {

            $.ajax({

                type:       'POST',
                url:        '/posts/ajax',
                data:       { num: 3, _token: "<?php echo csrf_token() ?>" },
                // dataType:   

                success:    function(data) {

                    console.log(data)    

                    let postsHtml = ''

                    for(i=0; i < data.posts.length; i++) {

                        const post = data.posts[i]

                        postsHtml += `
                            <div class="col-md-4 col-sm-4">
                                <div class="well well-lg">
                                    <h4> <a href="posts/${ post.id }"> ${ post.name } </a> </h4>
                                    <p> <b> Author: <span style="color:rgb(1, 97, 1)">${ post.author.name }</span> </b> </p>                                    
                                    <p> ${ post.body } </p>
                                </div>
                            </div>`
                    }

                   $('#homePosts').html(postsHtml)
                   $('#homePostsBtn').hide()
                },

                error:      function(error) {
                    console.error({error})
                }

            });
        }
    </script>

    <br/>
    <br/>

    <div class="row pad-botm">
        <div class="col-md-12">
            <h4 class="header-line">{{ $count_posts }} latest Posts</h4>
        </div>
    </div>
    <button id="homePostsBtn" class='btn btn-primary' onclick="getPosts()">Show latest posts</button>
    <div id="homePosts" class="row">

        {{-- @each('layouts.post', $posts, 'post') --}}
        
    </div>
    <!-- /. ROW  -->


    <br/>
    <br/>
    <div class="row pad-botm">
        <div class="col-md-12">
            <h4 class="header-line">SHOW POST EXAMPLE</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1>Jumbotron</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae
                    ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae
                    ultrices accumsan. Aliquam ornare lacus adipiscing.</p>
                <p>
                    <a class="btn btn-primary btn-lg" role="button">Edit</a>
                    <a class="btn btn-primary btn-lg" role="button">Delete</a>
                </p>
            </div>
        </div>
    </div>
    <!-- /. ROW  -->

    <div class="row pad-botm">
        <div class="col-md-12">
            <h4 class="header-line">CREATE/EDIT POST FORM EXAMPLE</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    BASIC FORM
                </div>
                <div class="panel-body">
                    <form role="form">
                        <div class="form-group">
                            <label>Enter Name</label>
                            <input class="form-control" type="text" />
                            <p class="help-block">Help text here.</p>
                        </div>
                        <div class="form-group">
                            <label>Enter Email</label>
                            <input class="form-control" type="text" />
                            <p class="help-block">Help text here.</p>
                        </div>
                        <div class="form-group">
                            <label>Text area</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>


                        <button type="submit" class="btn btn-info">Send Message </button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/.ROW-->

@endsection
