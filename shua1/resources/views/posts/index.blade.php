@extends('layouts.layout')

@section('content')

    {{-- <h1>Greeting: {{ $greeting }}</h1> --}}

    <br/>
    <br/>

    <div class="row pad-botm">

        <div class="col-md-12">
            <h4 class="header-line">{{ $count_posts }} POSTS {!! $tag ? ' of tag: <span class="tag">' . $tag->name . '</span>' : '' !!}</h4>
        </div>

        @auth
            <div style="display:flex; justify-content: flex-end">
                <a class="btn btn-primary btn-lg" role="button" href="{{ route('posts.create') }}">+ Add Post</a>
            </div>
        @endauth

    </div>
    <div class="row">

        @forelse($posts as $post)

            @include('layouts.post', ['post', $post])

        @empty

            <p>there is no Posts yet</p>

        @endforelse

    </div>
    <!-- /. ROW  -->

@endsection
