@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">

                <h1>{{ $post->name }}</h1>
                <p><b>Author: <span style="color:rgb(1, 97, 1)">{{ $post->author->name }}</span></b></p>
                <p>{{ $post->body }}</p>

                <div style="display:flex">
                    @foreach($post->tags as $tag)
                        <a href="/posts?tag={{$tag->id}}" style="text-decoration:    none;">
                            <p class="tag">{{ $tag->name }}
                            </p>
                        </a>
                    @endforeach
                </div>
                
                @auth
                    @if($post->author == Auth::user())
                        <div style="display:flex; justify-content: flex-end">

                            <div style="margin: 0 5px; padding: 2px 5px;">
                                <a class="btn btn-primary btn-lg" role="button"
                                    href="{{ route('posts.edit', [ 'post' => $post ] ) }}">Edit</a>
                            </div>

                            <div style="margin: 0 5px; padding: 2px 5px;">
                                <form role="form" action="{{ route('posts.destroy', [ 'post' => $post ] ) }}" method='POST'>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-primary btn-lg">Delete </button>
                                </form>
                            </div>

                        </div>
                    @endif
                @endauth

            </div>
        </div>
    </div>
    <!-- /. ROW  -->

@endsection
