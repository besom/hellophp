@extends('layouts.layout')

@section('content')

<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Create Post
            </div>

            @auth
            <div class="panel-body">
                <form role="form" action="{{ route('posts.store') }}" method='POST'>

                    @csrf

                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control @error('name') error-field @enderror" type="text" name='name'
                            value="{{ old('name') }}" required />
                        @error('name')
                        {{-- @if($errors->has('name')) --}}
                        <p class="error">
                            {{ $errors->first('name') }}
                        </p>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>Text</label>
                        <textarea class="form-control @error('body') error-field @enderror" rows="3" name='body'
                            required>{{ old('body') }}</textarea>
                        @error('body')
                        <p class="error">
                            {{ $errors->first('body') }}
                        </p>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>Select tags</label>
                        <select multiple name="tags[]" class="form-control">
                            @foreach($tags as $tag)
                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                            @endforeach
                        </select>

                        @error('tags')
                            <p class="error"> {{ $errors->first('tags') }} </p>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-info">Create </button>

                </form>
            </div>
            @else
            <script>
                window.location = "{{ route('posts.index') }}";
            </script>
            @endauth
        </div>
    </div>
</div>
<!--/.ROW-->

@endsection
