@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Edit Post
                </div>

                @auth
                    @if($post->author == Auth::user())

                        <div class="panel-body">
                            <form role="form" action="{{ route('posts.update', $post->id) }}" method='POST'>

                                @csrf
                                @method('PUT')

                                <div class="form-group">
                                    <label>Name</label>
                                    <input 
                                        class="form-control @error('name') error-field @enderror" 
                                        type="text" 
                                        name='name' 
                                        value="{{ old('name', $post->name) }}"
                                        required/>   
                                    @error('name')
                                        <p class="error">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @enderror                         
                                </div>
                                
                                <div class="form-group">
                                    <label>Text</label>
                                    <textarea 
                                        class="form-control @error('body') error-field @enderror" 
                                        rows="3" 
                                        name='body'
                                        required>{{ old('body', $post->body) }}</textarea>
                                    @error('body')
                                        <p class="error">
                                            {{ $errors->first('body') }}
                                        </p>
                                    @enderror 
                                </div>

                                <div class="form-group">
                                    <label>Select tags</label>
                                    <select multiple name="tags[]" class="form-control">
                                        @if(old('tags'))
                                            @foreach($tags as $tag)
                                                <option value="{{ $tag->id }}" {{ in_array($tag->id, old('tags')) ? 'selected' : '' }}>{{ $tag->name }}</option>
                                            @endforeach
                                        @else
                                            @foreach($tags as $tag)
                                                <option value="{{ $tag->id }}" {{ $post->tags->contains($tag) ? 'selected' : '' }}>{{ $tag->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
            
                                    @error('tags')
                                        <p class="error"> {{ $errors->first('tags') }} </p>
                                    @enderror
                                </div>

                                <button type="submit" class="btn btn-info">Update </button>

                            </form>
                        </div>
                    @else
                        <script>window.location = "{{ route('posts.index') }}";</script>
                    @endif
                @endauth

            </div>
        </div>
    </div>
    <!--/.ROW-->

@endsection
