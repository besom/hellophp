@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">

            <div class="panel panel-info">

                <div class="panel-heading">
                    Send Mail to admin
                </div>
                
                    <div class="panel-body">
                        <form role="form" action="{{ route('contact.store') }}" method='POST'>

                            @csrf
                            
                            <div class="form-group">
                                <label>Mail Content</label>
                                <textarea 
                                    class="form-control @error('mailbody') error-field @enderror" 
                                    rows="3" 
                                    name='mailbody'
                                    required>{{ old('mailbody') }}</textarea>
                            </div>
                            @error('mailbody')
                                <p class='error'>{{ $errors->first('mailbody') }}</p>
                            @enderror


                            <button type="submit" class="btn btn-info">Send </button>

                        </form>
                    </div>
                    
            </div>
            
        </div>
    </div>
    <!--/.ROW-->

@endsection
