@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">

            <div class="panel panel-info">

                <div class="panel-heading">
                    Notifications
                </div>
                
                <ul> 

                    @forelse($notifications as $notification)

                        <li>{{ $notification->type }}: {{ $notification->data['post']['name'] }} </li> 

                    @empty
                    
                        <li>There are no new notifications for you</li>

                    @endforelse
                                           
                </ul>
            </div>

            <div class="panel panel-info">

                <div class="panel-heading">
                    Old Notifications
                </div>
                
                <ul> 

                    @forelse($oldNotifications as $notification)

                        <li>{{ $notification->type }}: {{ $notification->data['post']['name'] }} </li> 

                    @empty
                    
                        <li>There is nothing for you</li>

                    @endforelse
                                           
                </ul>
            </div>
            
        </div>
    </div>
    <!--/.ROW-->

@endsection
