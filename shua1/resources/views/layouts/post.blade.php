<div class="col-md-4 col-sm-4">
    <div class="well well-lg">
        <h4>
            {{-- <a href="{{ route('posts.show', $post->id ) }}">
            <a href="{{ route('posts.show', $post ) }}"> --}}
                <a href="{{ $post->path() }}">
                    {{ $post->name }}
                </a>
        </h4>
        <p><b>Author: <span style="color:rgb(1, 97, 1)">{{ $post->author->name }}</span></b></p>
        <p>
            {{ $post->body }}
        </p>
    </div>
</div>
