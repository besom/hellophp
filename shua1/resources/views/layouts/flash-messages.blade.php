@if ($message = Session::get('success'))
    @include('layouts.flash-message', [
        'class'     => 'success alert-block',
        'message'   => $message
    ])
@endif

@if ($message = Session::get('error'))
    @include('layouts.flash-message', [
        'class'     => 'danger alert-block',
        'message'   => $message
    ])
@endif

@if ($message = Session::get('warning'))
    @include('layouts.flash-message', [
        'class'     => 'warning alert-block',
        'message'   => $message
    ])
@endif

@if ($message = Session::get('info'))
    @include('layouts.flash-message', [
        'class'     => 'info alert-block',
        'message'   => $message
    ])
@endif

@if ($errors->any())
    @include('layouts.flash-message', [
        'class'     => 'danger',
        'message'   => 'Please check the form below for errors'
    ])
@endif