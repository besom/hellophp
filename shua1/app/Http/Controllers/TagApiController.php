<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Tag;

class TagApiController extends Controller
{
    public function index()
    {
        return Tag::all();
    }

    public function posts(Tag $tag) 
    {
        return $tag->posts;
    }
}
