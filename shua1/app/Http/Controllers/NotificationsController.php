<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    public function show() 
    {
        $user = auth()->user();
        $oldNotifications = $user->readNotifications;
        $notifications = $user->unreadNotifications;
        $notifications->markAsRead();

        return view('notifications', [
            'notifications' => $notifications,
            'oldNotifications' => $oldNotifications,
            ]);
    }
}
