<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Post;

use App\Services\Standards;
use App\Services\Collaborator;

use ST;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    // public function index(Standards $st)
    {
        // $app_name       = config('app.name');
        // $colaborator    = new Collaborator();
        // $st             = new Standards($app_name, app(Collaborator::class));

        // $st             = app()->make(Standards::class);
        // $st             = resolve(Standards::class);        
        // $st             = app(Standards::class);

        // $greeting       = $st->greeting();

        $greeting       = ST::greeting();

        $posts          = Post::latest()->take(3)->get();
        // $count_posts    = Standards::count_posts($posts);
        $count_posts    = ST::count_posts($posts);
        
        return view('home', [ 
            'posts'         => $posts, 
            'greeting'      => $greeting,
            'count_posts'   => $count_posts
        ]);
    }
}
