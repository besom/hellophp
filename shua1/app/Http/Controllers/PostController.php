<?php

namespace App\Http\Controllers;

use Session;

use App\Models\Tag;
use App\Models\Post;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Notification;
use Notification;
use ST;

use App\Notifications\PostCreated;
use App\Notifications\PostEdited;
use App\Notifications\PostDeleted;

class PostController extends Controller
{
    // -------------------------------------------------------------
    // მაჩვენე ყველა პოსტის ჩამონათვალი
    // Display a listing of the resource.
    public function index()
    {
        $tag_id         = request('tag');

        if($tag_id) {
            $tag        = Tag::where('id', $tag_id)->firstOrFail();
            $posts      = $tag->posts;
        }
        else
            $posts      = Post::latest()->get();

        $greeting       = ST::greeting();
        $count_posts    = ST::count_posts($posts);

        return view('posts.index', [
            'tag'           => $tag ?? '',
            'posts'         => $posts, 
            'greeting'      => $greeting,
            'count_posts'   => $count_posts
        ]);
    }    
    
    public function indexAjax()
    {        
        request()->validate([
            'num' => ['required', 'integer']
        ]);

        $num    = request('num');
        $posts  = Post::latest()->take($num)->with("author")->get();
        
        return response()->json(array('posts' => $posts), 200);
    }

    // -------------------------------------------------------------
    // მაჩვენე ფორმა ახალი პოსტის შესაქმნელად
    // Show the form for creating a new resource.
    public function create()
    {
        return view('posts.create', [
            'tags' => Tag::all()
        ]);
    }

    // -------------------------------------------------------------
    // შექმენი პოსტი
    // Store a newly created resource in storage.
    public function store(Request $request)
    {
        // ---------------------
        // $post = new Post();        
        // $request->validate([
        //     'name' => ['required', 'min:3', 'max:45'],
        //     'body' => ['required', 'min:3'],
        // ]);
        // $post->name = request('name');
        // $post->body = request('body');

        // ---------------------
        $this->validatePost();
        $post = new Post(request(['name', 'body']));
        
        $user = request()->user();
        $post->user_id = $user->id;
        $post->save();

        $post->tags()->attach(request('tags'));

        // Notification::send($user, new PostCreated($post));
        $user->notify(new PostCreated($post));

        return 
            redirect()->
            route('posts.show', $post->id )->
            with('info', ST::post_action_info('creation'));
    }

    // -------------------------------------------------------------
    // მაჩვენე კონკრეტული ერთი პოსტი
    // Display the specified resource.
    public function show(Post $post)
    {
        return view('posts.show', ['post' => $post]);
    }

    // -------------------------------------------------------------
    // მაჩვენე ფორმა არსებული პოსტის შესაცვლელად
    // Show the form for editing the specified resource.
    public function edit(Post $post)
    {
        return view('posts.edit', [ 
            'post' => $post,
            'tags' => Tag::all()
        ]);
    }

    // -------------------------------------------------------------
    // შეცვალე პოსტი
    // Update the specified resource in storage. 
    public function update(Request $request, Post $post)
    {
        // ---------------------
        // request()->validate([
        //     'name' => ['required', 'min:3', 'max:45'],
        //     'body' => ['required', 'min:3'],
        // ]);
        // $post->name = request('name');
        // $post->body = request('body');
        // $post->save();

        // ---------------------
        // $validatedPostData = $this->validatePost(); // dd($validatedPostData);
        // $post->update($validatedPostData);

        // ---------------------
        $this->validatePost();
        $post->update(request(['name', 'body']));
        $post->tags()->sync(request('tags'));

        request()->user()->notify(new PostEdited($post));

        return 
            redirect()->
            route('posts.show', $post->id)->
            with('info', ST::post_action_info('edition'));
    }

    // -------------------------------------------------------------
    // წაშალე პოსტი
    // Remove the specified resource from storage.
    public function destroy(Post $post)
    {
        $post->delete();

        request()->user()->notify(new PostDeleted($post));

        return 
            redirect()->
            route('posts.index')->
            with('info', ST::post_action_info('deletion'));
    }

    // -------------------------------------------------------------
    private function validatePost() {
        return request()->validate([
            'name' => ['required', 'min:3', 'max:45'],
            'body' => ['required', 'min:3'],
            'tags' => ['exists:tags,id']
        ]);
    }

}
