<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use \App\Mail\ContactMail;

class ContactController extends Controller
{
    public function show()
    {
        return view('contact');
    }

    public function store(Request $request)
    {   
        request()->validate([
            'mailbody' => ['required', 'min:3']
        ]);
        
        $mailbody = request('mailbody');

        // Mail::raw($mailbody, function ($message) {
        //     $contactMail = config('mail.from.address');
        //     $message
        //       ->to($contactMail)
        //       ->subject('Mail from Custommer');
        //   });

        Mail::send(new ContactMail($mailbody));

        return redirect()->route('contact.show')->with('success', 'mail sent!');
    }

}
