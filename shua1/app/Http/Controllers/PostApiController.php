<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Post;

class PostApiController extends Controller
{
    public function index()
    {
        return Post::latest()->get();
    }

    public function show(Post $post) 
    {
        return $post;
    }

}
