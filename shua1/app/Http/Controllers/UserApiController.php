<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

class UserApiController extends Controller
{
    public function index()
    {
        return User::all();
    }

    public function posts(User $author) 
    {
        return $author->posts;
    }
    
}
