<?php

namespace App\Services;

use Session;
use App\Services\Collaborator;

class Standards
{
    private $app_name;
    private $collaborator;

    const GREETING_TEXT = "Hi I'm your app ";

    public function __construct($app_name, Collaborator $collaborator) {
        $this->app_name         = $app_name;
        $this->collaborator     = $collaborator;
    }

    public function greeting() {
        return self::GREETING_TEXT . strtoupper($this->app_name);
    }
    
    public static function count_posts($posts) {
        return count($posts);
    }

    private function action_number($action) {

        $number = 1;
        $key = $action.'_number';

        if (Session::has($key))
            $number += Session::get($key);

        Session::put($key, $number);

        return $number;
    }

    public function post_action_info($action) {
        $action_number = $this->action_number($action);
        return "post $action number: $action_number";
    }

}

?>