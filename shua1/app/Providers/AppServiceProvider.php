<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\Standards;
use App\Services\Collaborator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Standards::class, function() {
        // $this->app->singleton(Standards::class, function() {
            $app_name       = config('app.name');
            // $colaborator    = new Collaborator();
            return new Standards($app_name, app(Collaborator::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
