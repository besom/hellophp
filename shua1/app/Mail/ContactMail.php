<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $mailbody;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailbody)
    {
        $this->mailbody = $mailbody;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $appMail = config('mail.from.address');
        $from = auth()->user() ? auth()->user()->email : $appMail;

        return 
            $this
                ->to($appMail)
                ->from($from)
                ->subject('customer mail')
                ->view('mails.contact')
                ->with('mailbody', $this->mailbody);
    }
}
