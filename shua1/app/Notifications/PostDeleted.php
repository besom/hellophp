<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PostDeleted extends Notification
{
    use Queueable;

    private $post;

    public function __construct($post)
    {
        $this->post = $post;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Post is deleted!')
                    ->greeting('Wats Up?')
                    ->line('The Post name is: "' . $this->post->name . '"')
                    ->action('Go To Posts', url(route('posts.index')))
                    ->line('Thank you for using our application!');
    }


    public function toArray($notifiable)
    {
        return [
            'post' => $this->post
        ];
    }
}
