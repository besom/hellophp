<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PostCreated extends Notification
{
    use Queueable;

    private $post;

    public function __construct($post)
    {
        $this->post = $post;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Post is created!')
                    ->greeting('Wats Up?')
                    ->line('The Post name is: "' . $this->post->name . '"')
                    ->action('Go To Post', url(route('posts.show', $this->post)))
                    ->line('Thank you for using our application!');
    }


    public function toArray($notifiable)
    {
        return [
            'post' => $this->post
        ];
    }
}
