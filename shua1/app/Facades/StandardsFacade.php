<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use App\Services\Standards;

class StandardsFacade extends Facade 
{

    protected static function getFacadeAccessor() 
    { 
        return Standards::class; 
    }
}

?>