<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Tags table
        Schema::create('tags', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        // linking table between Posts and Tags
        Schema::create('post_tag', function (Blueprint $table) {

            $table->id();
            $table->unsignedBigInteger('post_id'); 
            $table->unsignedBigInteger('tag_id'); 
            $table->timestamps();

            // unique constraint for post_id and tag_id couple
            $table->unique(['post_id','tag_id']);

            // foreign key for post_id and tag_id
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->foreign('tag_id') ->references('id')->on('tags') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
