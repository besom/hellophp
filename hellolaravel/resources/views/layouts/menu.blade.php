<!-- header start -->   
<div class="header">
    <div class="container">
        <!--  header inner -->
        <div class="col-sm-12">
            <div class="menu-area">
                <nav class="navbar navbar-expand-lg ">
                    <!-- <a class="navbar-brand" href="#">Menu</a> -->
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href={{ route(config('constants.HOME')) }}>HOME<span class="sr-only">(current)</span></a> 
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href={{ route(config('constants.ABOUT')) }}>ABOUT</a>
                            </li>
                            <li class="#" href="#">
                                <a class="nav-link" href={{ route(config('constants.POSTS')) }}>BLOG</a>
                            </li>
                            <li class="nav-item" href="#">
                                <a class="nav-link" href={{ route(config('constants.CONTACT')) }}>CONTACT</a>
                            </li>
                            <li class="last"><a href="#"><img src="/images/search-icon.png" alt="icon"></a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- header end -->
