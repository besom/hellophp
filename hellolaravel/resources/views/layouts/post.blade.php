<div class="col-sm-4">
    <div class="section_1">
        {{-- <a href="{{ route(config('constants.POSTS'), [ 'id' => $post->id ] ) }}"> --}}
        {{-- <a href="{{ route(config('constants.POST'), $post ) }}"> --}}
        <a href="{{ $post->path() }}">
            <div><img src="images/code.jpg" style="max-width: 100%;"></div>
            <button type="button" class="date-bt">{{ $post->name }}</button>
        </a>
        <p>{{ $post->body }}</p>
    </div>
</div>