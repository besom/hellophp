@extends('layouts.layout')

@section('head')
    {{-- @include('layouts.bootstrap') --}}
@endsection

@section('content')

    <form action="{{ route(config('constants.POSTS') . ".update", $post) }}" method="POST">

        @csrf
        @method('PUT')

        <label for="fname">Post name:</label> <br>
        <input type="text" id="name" name="name" value="{{ $post->name }}"> <br>

        <label for="lname">Psts description:</label> <br>
        <textarea id="body" name="body" rows="4" cols="50">{{ $post->body }}</textarea> <br>

        <br>
        <input type="submit" value="Update">

    </form> 
    
@endsection
