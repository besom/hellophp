@extends('layouts.layout')

@section('head')
    {{-- @include('layouts.bootstrap') --}}
@endsection

@section('content')

    <!-- Blog Start -->
    <div class="blog_main">
    	<div class="container">
            <h1><button><a class="nav-link" href={{ route(config('constants.POSTS') . ".create") }}>+ Add Post</a></button></h1>
    		<div class="row">
    			<div class="col-sm-12">
    				<h1 class="blog_text">Our Blog</h1>
    			</div>
    	    </div>
    	    <div class="blog_section_2">
    	    	<div class="row">

                    {{-- @foreach($posts as $post)    
                        <div class="col-sm-4">
                            <div class="section_1">
                                <a href="{{ route(config('constants.POST'), [ 'id' => $post['id'] ]) }}">
                                    <div><img src="images/code.jpg" style="max-width: 100%;"></div>
                                    <button type="button" class="date-bt">{{ $post['name'] }}</button>
                                </a>
                                <p>{{ $post['body'] }}</p>
                            </div>
                        </div>
                    @endforeach

                    @foreach($posts as $post)
                        @include('layouts.post', ['post' => $post])     
                    @endforeach --}}

                    @each('layouts.post', $posts, 'post')
                    
    	    	</div>
    	    </div>
       	</div>
    </div> 
    <!--blog end -->

@endsection
