@extends('layouts.layout')

@section('head')
    {{-- @include('layouts.bootstrap') --}}
@endsection

@section('content')

    <div class="blog_main">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
    				<h1 class="blog_text">{{ $post->name ?? '' }}</h1>
    				<h5>{{ $post->body ?? '' }}</h5>
    			</div>
    	    </div>
            <h5>
                {{-- <button><a class="nav-link" href={{ route(config('constants.POSTS') . ".edit", $post) }}>Edit Post</a></button> --}}
                <form action="{{ route(config('constants.POSTS') . ".edit", $post) }}" method="GET">
                    <input type="submit" value="Edit Post">            
                </form> 
                <form action="{{ route(config('constants.POSTS') . ".destroy", $post) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete Post">            
                </form> 
            </h5>
       	</div>
    </div> 
    
@endsection
