<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    // protected $table = 'posts'; // შესაბამისი ცხრილის სახლი მონაცემთა ბაზაში
    // protected $primaryKey = 'id'; // ცხრილის პირველადი გასაღები 
    // public $timestamps = true; // შეივსოს თუ არა ცხრილის created_at და updated_at ველები ავტომატურად, ცხრილთან მუშაობისას.
    
    // მომდევნო ორი თვისებიდან ერთერთი მაინც უნდა იყოს განსაზღვრული რომ შევძლოთ მოდელიდან ცხრილში ჩანაწერის შექმნა create მეთოდის გამოყენებით
    protected $fillable = ['name','body']; // ცხრილში ველები რომელშიც შესაძლებელია ინფორმაციის შეტანა
    // protected $guarded = ['id']; // ცხრილში ველები რომელშიც შეუძლებელია ინფორმაციის შეტანა

    public function path() {
        return route(config('constants.POSTS') . ".show", $this);
    }

}
