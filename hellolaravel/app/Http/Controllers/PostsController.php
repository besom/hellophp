<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;


class PostsController extends Controller
{

    // მაჩვენე ყველა პოსტის ჩამონათვალი
    // Display a listing of the resource.
    public function index() 
    {
        // $posts = Post::all();
        $posts = Post::latest()->get();

        // return 
        //     view(
        //         'posts.index', //config('constants.POSTS'), 
        //         [ 'posts' => $posts]
        //     );

        // latest()->get() ფუნქციაც წამოიღებს ყველა პოსტს, 
        // უბრალოდ დაალაგებს created_at ველის მიხედვით, ჯერ ახლებს მერე ძველებს 
        return 
            view(
                'posts.index', //config('constants.POSTS') . ".index", 
                [ 'posts' => $posts]
            ); 
    }

    // -----------------------------------------------------------------------------------------------------------------

    // მაჩვენე ფორმა ახალი პოსტის შესაქმნელად
    // Show the form for creating a new resource.
    public function create()
    {
        return 
            view(
                config('constants.POSTS') . ".create"
            ); 
    }

    // -----------------------------------------------------------------------------------------------------------------

    // შექმენი პოსტი
    // Store a newly created resource in storage.
    public function store(Request $request)
    {
        $post = new Post();

        // $post->name = $request->input('name');
        // $post->body = $request->input('body');
        $post->name = request('name');
        $post->body = request('body');

        $post->save();

        return 
            redirect()->
                route(
                    config('constants.POSTS') . ".show", 
                    $post
                );
    }

    // -----------------------------------------------------------------------------------------------------------------

    // მაჩვენე კონკრეტული ერთი პოსტი
    // Display the specified resource.
    //
    // ბრაუზერში პოსტის მისამართის აკრეფისას, ბოლოში ვუთითებთ პოსტის ID_ს =>        http://localhost:8000/posts/16
    // როუტის აღწერისას მისამართის ID_ის ნაწილი გამოცხადებულია როგორც პარამეტრი =>     Route::get("$posts/{id}", 'PostsController@show');
    // ჩვეულებრივ ეს პარამეტრი გადმოეცემა როუტში აღწერილ კონტროლერ ფუნქციას =>         public function show($id) { //... }
    // public function show( $id)
    // {
    //     $post = Post::findOrFail($id);

    //     return 
    //         view(
    //             config('constants.POSTS') . ".show", 
    //             [ 'post' => $post ]
    //         );
    // }
    // თუ ვიტყვი რომ კონტროლერში პარამეტრად ველოდები პირდაპირ Post მოდელის ობიექტს
    // მაშინ ლარაველი როუტში გადმოცემული ID_ით თავად შექმნის Post მოდელის ობიექტს და გადმომცემს კონტროლერში პარამეტრად
    // ლარაველში ამ კომფორტს ქვია: Route Model Binding ანუ როუტის და მოდელის ბმა
    // WARNING: კრიტიკულია რომ როუტში აღწერილი პარამეტრის სახელი ემთხვეოდეს კონტროლერ ფუნქციის პარამეტრის სახელს
    //          Route::get("$posts/{post}", 'PostsController@show')
    //          function show(Post $post)
    public function show(Post $post)
    {
        return 
            view(
                config('constants.POSTS') . ".show", 
                [ 'post' => $post ]
            );
    }

    // -----------------------------------------------------------------------------------------------------------------

    // მაჩვენე ფორმა არსებული პოსტის შესაცვლელად
    // Show the form for editing the specified resource.
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        //
        return 
            view(
                config('constants.POSTS') . ".edit", 
                [ 'post' => $post ]
            ); 
    }

    // -----------------------------------------------------------------------------------------------------------------

    // შეცვალე პოსტი
    // Update the specified resource in storage. 
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        // $post->name = $request->input('name');
        // $post->body = $request->input('body');
        $post->name = request('name');
        $post->body = request('body');

        $post->save();
        
        return 
            redirect()->
                route(
                    config('constants.POSTS') . ".show", 
                    $id
                );
    }

    // -----------------------------------------------------------------------------------------------------------------

    // წაშალე პოსტი
    // Remove the specified resource from storage.
    public function destroy(Post $post)
    {
        // $post = Post::findOrFail($id);

        $post->delete();

        return 
            redirect()->
                route(
                    config('constants.POSTS')
                ); 
    }

}
