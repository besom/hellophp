<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\Post;

class PostsSeeder extends Seeder
{
    // const TABLE = 'posts';
    // const INSERT_QUERY = 'INSERT INTO '.self::TABLE.' (name, body) VALUES(?,?)';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::insert(self::INSERT_QUERY,
        // [
        //     'YOUR TEACHER - JEFFREY WAY',
        //     "Hi, I'm Jeffrey. I'm the creator of Laracasts and spend most of my days building the site and thinking of new ways to teach confusing concepts. I live in Orlando, Florida with my wife and two kids...."
        // ]);
        // //
        // DB::insert(self::INSERT_QUERY,
        // [
        //     'CRITIC',
        //     "I don't recommend Jeffrey Way for beginners. Look up youtube for laravel from crash course It's simpler to understand then when you become more proficient you can come back here. At first I had difficulty with Jeffrey but now I notice no difference and used to it. But took his courses for months to get used to his speed and guess what? I became a faster developer as well."
        // ]);
        // //
        // DB::insert(self::INSERT_QUERY,
        // [
        //     'CRITIC OF CRITICS',
        //     "You can slow down the video and you can always pause to look at te code. I do it all the time in this tutorial. I also often watch the video twice :) I like it that the videos are packed with useful information at every second, it saves tons of time."
        // ]);
        // //
        // DB::table(self::TABLE)->insert(
        //     [
        //             ['name' => 'test name', 'body' => 'test text'],
        //             ['name' => 'test name 1', 'body' => 'test text 1']
        //     ]
        //     );
        // //
        // Post::insert(
        //     [
        //             ['name' => 'test name 3', 'body' => 'test text 3'],
        //             ['name' => 'test name 4', 'body' => 'test text 4']
        //     ]
        //     );
        // //
        // // Create a new user in the database...
        // $post = Post::create(array('name' => 'John', 'body' => 'asdfasdfasdfasdfas asdf asdf asdf sdf adf asdf asd fadf'));
        // // Retrieve the user by the attributes, or create it if it doesn't exist...
        // $post = Post::firstOrCreate(array('name' => 'John', 'body' => 'asdfasdfasdfasdfas asdf asdf asdf sdf adf asdf asd fadf'));
        // // Retrieve the user by the attributes, or instantiate a new instance...
        // // $post = Post::firstOrNew(array('name' => 'John', 'body' => 'asdfasdfasdfasdfas asdf asdf asdf sdf adf asdf asd fadf'));
        // //
        // $post = new Post();
        // $post->name = 'test name 2';
        // $post->body = 'test text 2';
        // $post->save();
        //
        Post::factory()->count(5)->create();
    }
}