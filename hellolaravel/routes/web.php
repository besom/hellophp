<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$home =     config('constants.HOME');
$about =    config('constants.ABOUT');
$contact =  config('constants.CONTACT');
$posts =    config('constants.POSTS');
$post =     config('constants.POST');

// name() ფუნქციით დამუშავებულ მისამართს ვარქმევთ სახელს
// Route::currentRouteName() ფუნქციით ვიღებთ მიმდინარე მისამართის სახელს
Route::get($home,       function() { return view(Route::currentRouteName()); })->name($home);
Route::get($about,      function() { return view(Route::currentRouteName()); })->name($about);
Route::get($contact,    function() { return view(Route::currentRouteName()); })->name($contact);

// მითითებული ბრძანებით შესაძლოა კონტროლერის შექმნა, ამ საკითხს მოგვიანებით უფრო დაწვრილებით გავივლით
// php artisan make:controller PostsController
//
// იმისთვის რომ Route კლასზე გადაცემუი კონტროლერების სახელებს სწორი namespace_ით მივაკითხოთ
// app/Providers/RouteServiceProvider.php ფაილში განაკომენტარეთ შემდეგი ხაზი: protected $namespace = 'App\\Http\\Controllers';

Route::get(     $posts,             'PostsController@index'     )->name($posts);                // მაჩვენე ყველა რესურსის გვედრი 

Route::get(     "$posts/create",    'PostsController@create'    )->name("$posts.create");       // მაჩვენე ახალი რესურსის შესაქმნელი გვერდი
Route::post(    $posts,             'PostsController@store'     )->name("$posts.store");        // შექმენი რესურსი

// Route::get(     "$posts/{id}",      'PostsController@show'      )->name("$posts.show");         // მაჩვენე კონკრეტული რესურსის გვერდი
Route::get(     "$posts/{post}",    'PostsController@show'      )->name("$posts.show");

Route::get(     "$posts/{id}/edit", 'PostsController@edit'      )->name("$posts.edit");         // მაჩვენე კონკრეტული რესურსის შესაცვლელი გვერდი
Route::put(     "$posts/{id}",      'PostsController@update'    )->name("$posts.update");       // შეცვალე კონკრეტული რესურსი

Route::delete(  "$posts/{post}",      'PostsController@destroy'   )->name("$posts.destroy");      // წაშალე კონკრეტული რესურსი


// view('viewname')->render() ფუნქცია არენდერებს წარმოდგენას და მაძლევს მის HTML სორსს სტრინგად
Route::get('render', function() { 
    $render = view('welcome')->render();
    echo gettype($render); 
    dd($render);
});

// Route არის Laravel ფრეიმვორკის კლასი რომელიც გვეხმარება ჩვენ აპლიცაკიასთან მოსული მოთხოვნების დამუშავება გადამისამართებაში.
// ::get არის ამ კლასის მეთოდი რომელიც ამუშაევბს HTTP GET ტიპის მოთხოვნას, რომელსაც გადაეცემა ორი პარამეტრი
//// 1. '/' - მისამართი საიდანაც მოთხოვნა შემოვიდა
//// 2. ქოლბექ ფუნქცია რომელშიც ვამუშავებთ მოთხოვნას და ვაბრუნებთ პასუხს, ძირითადად რომელიმე წარმოდგენას
Route::get('/', function () { return redirect()->route('home'); }); // მოთხოვნას ვამისამართებთ home მისამართზე
// Route::get('/welcome', function(){ return view('welcome'); });

// // მოთხოვნა აბრუნებს უბრალოდ სტრინგს
// Route::get('/string', function () {
//     return "I'm just the string";
// });
// // მოთხოვნა აბრუნებს მასივს, რომელიც ავტომატურად json ად იკასტება
// Route::get('/arrayAsJson', function () {
//     return ['myKey1'=> 'myValue1', 'myKey2'=> 'myValue2'];
// });

// //<script>alert('I broke your code')</script>
// Route::get('myname', function () {

//     // მოთხოვნიდან get პარამეტრების ამოღება (აი მისამართში რომ კითხვის ნიშნის შემდეგ რომაა მითითებული ეგ პარამეტრები) 
//     // $name = $_GET['name'];
//     $name = request('name');

//     // ცვლადები შეგვიძლია გადავცეთ წარმოდგენას
//     return view('myname', [
//         'name' => $name
//     ]);

// });

// --------------------------------------------------------------------------------------------------------------------

// // მისამართის ნაწილი თავად შეიძლება იყოს პარამეტრი რომელიც ფიგურულ ფრჩხილებში ჩასმით აღინიშნება და რიგითობის მიხედვით გადაეცემა ქოლბექ ფუნქციას
// Route::get('posts/{name}', function ($name) {

//     $posts = array(
//         'first' => 'my first post...',
//         'second' => 'my second post...',
//         'third' => 'my third post...'
//     );

//     // isset: returns true if array key exists and is not null
//     // array_key_exists: returns true if array key exists
//     // if ( ! isset($posts[$post]))
//     if ( ! array_key_exists($name, $posts)) 
//         abort(404);

//     return view('posts', [
//         'name' => $name,
//         'post' => $posts[$name] ?? 'უკაცრავად, პოსტი ვერ მოიძებნა!'
//     ]);
// });

// // შევქმნათ public/postForm.html ფაილი და html form ტეგის საშუალებით დავპოსტოთ მონაცემები /newpost მისამართზე
// // Route::post('newpost', function() { // დავამუშაოთ HTTP POST ტიპის მოთხოვნა
// Route::match(['get','post'], 'newpost', function() { // დავამუშაოთ რამოდენიმე სხვადასხვა HTTP ტიპის მოთხოვნა ერთდროულად
// // Route::any('newpost', function() { // დავამუშაოთ ყველა HTTP ტიპის მოთხოვნა ერთდროულად

//     // მოთხოვნიდან post პარამეტრების ამოღება (აი მისამართში რო არ ჩანან და დამალულები რო არიან ეგ პარამეტრები)
//     // $name = $_POST['name'];
//     // $post = $_POST['post'];
//     $name = request('name');
//     $post = request('post');

//     return view('post', [
//         'post' => [
//             'id' => 0,
//             'name' => $name,
//             'body' => $post
//         ]
//     ]);

// });